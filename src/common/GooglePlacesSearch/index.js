// import React, {Component} from 'react';
// import {View} from 'react-native';
// import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';

// import mapstyles from './mapstyles';
// import styles from './styles';

// import {GOOGLE_MAP_KEY} from '../../config/Constants';
// import {strings} from '../../utils/i18n';

// import {Colors} from '../../theme';

// class GooglePlacesSearch extends Component {
//   render() {
//     const {onPress, onChangeText, initialRegion, placesRef} = this.props;
//     return (
//       <View style={styles.container}>
//         <GooglePlacesAutocomplete
//           bounces={false}
//           placeholder={strings('placeholders.searchLocation')}
//           minLength={2}
//           ListFooterComponent={() => {
//             return null;
//           }}
//           // autoFocus
//           listViewDisplayed="auto"
//           fetchDetails
//           ref={placesRef}
//           renderDescription={(row) => row.description}
//           onPress={onPress}
//           getDefaultValue={() => initialRegion}
//           query={{
//             key: GOOGLE_MAP_KEY,
//             language: 'en', // language of the results
//           }}
//           styles={mapstyles}
//           nearbyPlacesAPI="GooglePlacesSearch"
//           GooglePlacesSearchQuery={{}}
//           filterReverseGeocodingByTypes={[
//             'locality',
//             'administrative_area_level_3',
//           ]}
//           debounce={200}
//           textInputProps={{
//             clearButtonMode: 'never',
//             autoCapitalize: 'none',
//             autoCorrect: false,
//             selectionColor: Colors.fadedOrange,
//             onChangeText: onChangeText,
//           }}
//         />
//       </View>
//     );
//   }
// }

// export default GooglePlacesSearch;

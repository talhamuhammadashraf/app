// @flow
import {StyleSheet} from 'react-native';
import {Colors, Metrics, Fonts} from '../../theme';

const heightOffset = Metrics.isIphoneX ? 2 : 3;

export default StyleSheet.create({
  container: {
    flex: 0,
    backgroundColor: 'white',
  },
  textInputContainer: {
    alignItems: 'center',
    borderRadius: Metrics.ratio(8),
  },
  description: {
    color: Colors.black,
    fontFamily: Fonts.type.regular,
    fontSize: Fonts.size.size_16,
    marginTop: Metrics.ratio(10),
    paddingRight: Metrics.ratio(45),
  },
  row: {
    paddingLeft: Metrics.baseMargin,
    height: 60,
    backgroundColor: Colors.white,
  },
  separator: {
    height: 2,
    opacity: 0.2,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: Colors.warmGrey,
    marginHorizontal: Metrics.ratio(13),
  },
  textInput: {
    height: Metrics.doubleBaseMargin * heightOffset,
    paddingLeft: Metrics.ratio(18),
    marginTop: 0,
    marginLeft: 0,
    marginRight: 0,
    fontSize: Fonts.size.size_16,
    color: Colors.black,
    fontFamily: Fonts.type.regular,
    borderRadius: Metrics.ratio(8),
  },
});

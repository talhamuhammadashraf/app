import {StyleSheet} from 'react-native';
import {Metrics} from '../../theme';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: Metrics.navBarHeight * 0.8,
    backgroundColor: 'red',
    marginHorizontal: Metrics.ratio(22),
  },
});

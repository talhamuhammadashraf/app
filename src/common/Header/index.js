// import {View, Image, ImageBackground} from 'react-native';
// import React from 'react';
// import PropTypes from 'prop-types';
// import {ButtonView, Text} from '../../components';
// import {Images} from '../../theme';
// import {NavigationService} from '../../utils';
// import styles from './styles';

// export default class Header extends React.Component {
//   static propTypes = {
//     style: PropTypes.oneOfType([
//       PropTypes.object,
//       PropTypes.array,
//       PropTypes.number,
//     ]),
//     backgroundImage: PropTypes.string,
//     leftButton: PropTypes.string,
//     onLeftButtonPress: PropTypes.func,
//     title: PropTypes.string,
//     count: PropTypes.string,
//     rightButtons: PropTypes.array,
//   };

//   static defaultProps = {
//     style: {},
//     backgroundImage: undefined,
//     leftButton: undefined,
//     onLeftButtonPress: undefined,
//     title: undefined,
//     count: undefined,
//     rightButtons: [],
//   };

//   onBackPress = () => {
//     NavigationService.pop();
//   };

//   renderContent = () => (
//     <View style={styles.contentContainer}>
//       {this.renderLeft()}
//       {this.renderRight()}
//     </View>
//   );

//   renderLeft = () => (
//     <View style={styles.left}>
//       {this.renderLeftButton()}
//       {this.renderTitles()}
//     </View>
//   );

//   renderLeftButton() {
//     const {leftButton, onLeftButtonPress} = this.props;
//     const image = leftButton ? leftButton : Images.buttons.backArrow;
//     const onPress = onLeftButtonPress ? onLeftButtonPress : this.onBackPress;
//     return (
//       <ButtonView onPress={onPress}>
//         <Image source={image} />
//       </ButtonView>
//     );
//   }

//   renderTitles() {
//     const {title, count} = this.props;
//     return (
//       <View style={styles.titleContainer}>
//         {count >= 0 && this.renderCount(count)}
//         {title && this.renderTitle(title)}
//       </View>
//     );
//   }

//   renderTitle = (title) => (
//     <View style={styles.title}>
//       <Text
//         textAlign="center"
//         size="size_35"
//         color={'white'}
//         style={{lineHeight: 50}}>
//         {title}
//       </Text>
//     </View>
//   );

//   renderCount = (count) => (
//     <View style={styles.count}>
//       <Text
//         textAlign="center"
//         size="size_50"
//         type="bold"
//         color={'white'}
//         style={{lineHeight: 50}}>
//         {count}
//       </Text>
//     </View>
//   );

//   renderRight() {
//     const {rightButtons} = this.props;
//     return (
//       <View style={styles.right}>
//         {rightButtons && this.renderRightButtons(rightButtons)}
//       </View>
//     );
//   }

//   renderRightButtons(rightButtons) {
//     return rightButtons.map((item) => (
//       <ButtonView style={styles.rightButtonsMargin} onPress={item.onPress}>
//         <Image source={item.source}></Image>
//       </ButtonView>
//     ));
//   }

//   render() {
//     const {backgroundImage, style} = this.props;
//     if (backgroundImage) {
//       return (
//         <ImageBackground
//           source={backgroundImage}
//           style={[styles.containerWithImage, style]}>
//           {this.renderContent()}
//         </ImageBackground>
//       );
//     } else {
//       return (
//         <View style={[styles.container, style]}>{this.renderContent()}</View>
//       );
//     }
//   }
// }

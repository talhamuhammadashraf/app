import {StyleSheet} from 'react-native';
import {Metrics} from '../../theme';

export default StyleSheet.create({
  container: {
    padding: Metrics.baseMargin,
  },
  containerWithImage: {
    paddingTop: Metrics.ratio(55),
    paddingHorizontal: Metrics.baseMargin,
    height: Metrics.ratio(166),
  },
  contentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  left: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  right: {
    alignItems: 'center',
    flexDirection: 'row-reverse',
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: Metrics.baseMargin,
  },
  title: {
    marginRight: Metrics.ratio(5),
  },
  count: {
    marginRight: Metrics.ratio(5),
  },
  rightButtonsMargin: {
    marginLeft: Metrics.mediumMargin,
  },
});

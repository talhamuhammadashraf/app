import React from 'react';
import {MessageBar, MessageBarManager} from 'react-native-message-bar';
import {Metrics} from '../../theme';

export default class extends React.Component {
  componentDidMount() {
    MessageBarManager.registerMessageBar(this.refs.alert);
  }

  componentWillUnmount() {
    MessageBarManager.unregisterMessageBar();
  }

  render() {
    return <MessageBar ref="alert" viewTopInset={Metrics.MESSAGE_BAR_HEIGHT} />;
  }
}

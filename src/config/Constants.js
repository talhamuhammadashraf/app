// export const PIN_COUNT = 4;
// export const VERIFICATION_TIME = 60;
// export const GOOGLE_MAP_KEY = 'AIzaSyCBzfbEvIZAHCM9RL8KYvryNaYrNUn8bE8';
// export const PROFILE_FIELD_TYPE = {
//   DEFAULT: 1,
//   ARROW: 2,
//   TOGGLE: 3,
// };
// export const CHECK_BOX_CONTAINER_TYPE = {
//   ROW: 1,
//   COLMUN: 2,
// };
// export const CHECK_BOX_TYPE = {
//   PRIMARY: 1,
//   SECONDARY: 2,
// };
// export const RATING_NAVIGATION_TYPE = {
//   FROM_HOME: 1,
//   FROM_END: 2,
// };
// export const USERTYPE = {
//   CUSTOMER: 'user',
//   DRIVER: 'driver',
// };
// export const ORDER_STATUS = {
//   NEW: 'new',
//   PENDING: 'pending',
//   ACCEPTED: 'accepted',
//   On_GOING: 'ongoing',
//   HISTORY: 'history',
// };
// export const ONGOING_ORDER_STATUS = {
//   DELIVERED: 'delivered',
//   NOT_STARTED: 'notStarted',
//   ON_THE_WAY: 'onTheWay',
//   On_GOING: 'ongoing',
//   PICKED_UP: 'pickedUp',
// };

// export const GENERAL_ITEM_TYPE = {
//   ITEM_TYPE: 'item_type',
//   SERVICES: 'services',
// };

// export const IMAGE_COMPRESS_MAX_WIDTH = 720;

// export const PICKER_TYPE = {
//   // FOR CAMERA
//   CAMERA: 'CAMERA',
//   CAMERA_WITH_CROPPING: 'CAMERA_WITH_CROPPING',
//   CAMERA_BINARY_DATA: 'CAMERA_BINARY_DATA',
//   CAMERA_WITH_CROPPING_BINARY_DATA: 'CAMERA_WITH_CROPPING_BINARY_DATA',

//   // FOR GALLERY
//   GALLERY: 'GALLERY',
//   GALLERY_WITH_CROPPING: 'GALLERY_WITH_CROPPING',
//   GALLERY_BINARY_DATA: 'GALLERY_BINARY_DATA',
//   GALLERY_WITH_CROPPING_BINARY_DATA: 'GALLERY_WITH_CROPPING_BINARY_DATA',

//   // FOR MULTI PICK
//   MULTI_PICK: 'MULTI_PICK',
//   MULTI_PICK_BINARY_DATA: 'MULTI_PICK_BINARY_DATA',
// };

// // date time formats
// export const CONTACT_US_DATE_FORMAT = 'DD MMM YYYY';
// export const DATE_TIME_FORMAT = 'DD MMM YYYY | hh:mm A';
// export const DATE_TIME_FORMAT2 = 'MMM DD YYYY hh:mmh';
// export const DATE_FORMAT1 = 'MMMM DD, YYYY';
// export const DATE_FORMAT2 = 'DD MMM YYYY';
// export const DATE_FORMAT3 = 'MM/DD/YY';
// export const DATE_FORMAT4 = 'YYYY-DD-MM';
// export const DATE_FORMAT5 = 'D MMM YYYY';
// export const DATE_FORMAT6 = 'DD-MM-YYYY';
// export const DATE_FORMAT7 = 'DD MMM, YYYY';
// export const DATE_FORMAT8 = 'MM/DD/YYYY hh:mm:ss H';
// export const TIME_FORMAT1 = 'hh:mm A';
// export const TIME_FORMAT2 = 'HH:mm';

// export const CALENDAR_DAY_FORMAT = 'ddd';
// export const CALENDAR_DATE_FORMAT = 'DD';

// export const DATE_FORMAT = 'YYYY-MM-DD';
// export const DISPLAY_DATE_FORMAT = 'DD MMM, YYYY';
// export const DISPLAY_DATE_FORMAT_ORDER = 'D MMMM YYYY';
// export const DISPLAY_DATE_ORDER_FORMAT = 'DD MMM';
// export const YEAR_FORMAT = 'YYYY';
// export const EDIT_PROFILE_TYPE = {
//   EMAIL: 'email',
//   PASSWORD: 'password',
//   MOBILE_NUMBER: 'mobileNumber',
// };
// export const ORDER_TYPE = {
//   SHIPMENT: 1,
//   LOCATION: 2,
//   TIME: 3,
//   SERVICE: 4,
//   REVIEW: 5,
// };
// export const DRIVER_LICENSE_TYPE = {
//   ACTIVE: 'ACTIVE',
//   IN_ACTIVE: 'IN_ACTIVE',
//   BLOCKED: 'BLOCKED',
// };

// export const SHIPMENT_CATEGORY_TYPE = {
//   PLANTS: 1,
//   FOOD: 2,
//   FURNITURE: 3,
//   WATER: 4,
//   GAS: 5,
// };

// export const DUMMY_LOCATION_DATA = {
//   fromLocation:
//     'Address: 1004, Fortune Executive towers, Jumeirah lakes tower, Dubai',
//   toLocation:
//     'Address: 1005, Fortune Executive towers, Jumeirah lakes tower, Dubai',
// };

// // custom, facebook, gmail
// export const USER_AUTH_TYPE = {
//   CUSTOM: 'custom',
//   FACEBOOK: 'facebook',
//   INSTAGRAM: 'instagram',
//   GMAIL: 'gmail',
//   APPLE: 'apple',
//   TWITTER: 'twitter',
// };
// export const REQUEST_METHOD = {
//   POST: 'POST',
//   FILE: 'FILE',
//   GET: 'GET',
//   PUT: 'PUT',
//   DELETE: 'DELETE',
//   PATCH: 'PATCH',
// };
// export const USER_NAV_TYPE = {
//   SIGNUP: 'SIGNUP',
//   FORGET_PASSWORD: 'FORGET_PASSWORD',
// };

// export const CMS_CONTENT_TYPES = {
//   termsAndCondition: 'terms-and-conditions',
// };

// Google map route
// export const GOOGLE_BASE_URL = 'https://maps.googleapis.com';
export const GOOGLE_API_KEY = ''; // AIzaSyCBzfbEvIZAHCM9RL8KYvryNaYrNUn8bE8
export const API_GOOGLE_DIRECTION = '/maps/api/directions/json';

// staging
// export const BASE_URL = "http://node.cubix.co:3006/";
export const BASE_URL = 'http://node.cubix.co:7003/';
export const FRAME_BASE_URL = 'http://node.cubix.co:3006';
export const BASE_AUDIO_URL =
  'http://sandbox4.cubix.co/staging/stepapp/storage/app/public/';

// Share Urls
export const PRODUCTS_URL = `${BASE_URL}products`;

// CONSTANTS
export const LIMIT = 10;

// Share Urls
export const STATION_SHARE_URL = `${BASE_URL}/view-station/`;

// Payment Card Image URL
export const PAYMENT_CARD_IMAGE_URL = `${BASE_URL}/paymentCards/`;

// One time Payment URL
export const PAYMENT_URL = `${BASE_URL}/api/oomco_payment/pay`;

// Services Image Url
export const SERVICES_IMAGE_URL = `${BASE_URL}/icons/`;
export const SERVICES_IMAGE_BLACK_PNG = '_black.png';
export const SERVICES_IMAGE_WHITE_PNG = '_white.png';

export const X_Access_Token = '3q99oljkjwhomz8m3q99oljkjwhomz8n';

export const API_TIMEOUT = 60000;
export const API = '/api/v1';

export const API_USER_EXIST = 'verify_email';
export const API_USER_EMAIL_PIN_VERIFICATION = 'verify_email_code';

export const API_POST_SIGNUP = 'user';
export const API_POST_LOGIN = 'login';
export const API_POST_VERIFY_SIGNUP_CODE = 'verify_code_signup';
export const API_POST_VERIFY_EMAIL_PHONE_CODE = 'verify_email_phone_code';
export const API_POST_LOGOUT = 'Tokens';
export const API_GET_FAQS = 'faqs'; // supportThreads
export const API_GET_CONTACT_US = 'supportThreads'; // supportThreads
export const API_GET_CMS_CONTENT = 'cms';

export const API_POST_VERIFY_FORGOT_PASSWORD = 'send_email_phone_code';
export const API_POST_RESET_PASSWORD = 'resetPass';
export const API_CHECK_REFERRAL_CODE = 'validate-referral-code';

export const API_POST_FORGOT_PASSWORD = 'verify_change_email_code';
export const API_POST_CHANGE_PASSWORD = 'change-password';
export const API_POST_FILE_UPLOAD = 'file/upload';
export const API_POST_UPDATE_PROFILE = 'user'; // mark-tutorial-completed
export const API_POST_MARK_TUTORIAL_COMPLETE = 'mark-tutorial-completed'; // mark-tutorial-completed

export const API_POST_CREATE_GIVITY = 'givity';
export const API_GET_GIVITY_DETAIL = 'givity/';
export const API_GET_LYRICS_AND_AUDIO = 'LyricsAndAudio';
export const API_GET_LYRICS_LANGUAGE = 'LyricsLanguage';
export const API_GET_STORE_GIFTS = 'dashboard';

export const API_GET_ITEMTYPES = 'itemTypes';
export const API_GET_SERVICES = 'services';
export const API_UPLOAD_FILE = 'file/upload';

// API USER ROUTES
export const API_LOG = true;

export const ACCESS_TOKEN_EXPIRED = {
  message: 'Token Expired',
  error: 1,
};

export const ERROR_SOMETHING_WENT_WRONG = {
  message: 'Oops! Something went wrong',
  error: 1,
};
export const ERROR_NETWORK_NOT_AVAILABLE = {
  message: 'Internet connection error.',
  error: 1,
};
export const TOKEN_EXPIRE = {
  token_expired: true,
  error: 1,
};

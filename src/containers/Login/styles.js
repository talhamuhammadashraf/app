import {StyleSheet} from 'react-native';
import {Metrics} from '../../theme';

export default StyleSheet.create({
  container: {
    paddingTop: Metrics.ratio(28),
    paddingBottom: Metrics.ratio(30),
    paddingHorizontal: Metrics.mediumMargin,
  },
  password: {
    marginTop: Metrics.ratio(30),
    marginBottom: Metrics.ratio(56),
  },
  loginButton: {
    marginTop: Metrics.ratio(50),
    marginBottom: Metrics.ratio(42),
  },
  forgotButton: {
    alignSelf: 'center',
  },
});

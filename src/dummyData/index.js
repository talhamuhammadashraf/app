import {Images} from '../theme';
import {ONGOING_ORDER_STATUS, DRIVER_LICENSE_TYPE} from '../config/Constants';

export const customerPendingOrders = [
  {
    id: '90380938',
    name: 'John Doe',
    orderType: 'Furniture',
    remainingTime: '54:12',
    orderDate: '28 AUG 2020',
    responses: [
      {
        name: 'The Golden Transportation',
        image: Images.dummyImages.driverAvatar,
        rating: 3,
        price: '800',
        remainingTime: '54:12',
      },
      {
        name: 'Company name',
        image: Images.dummyImages.driverAvatar2,
        rating: 4,
        price: '1000',
        timeOut: true,
        remainingTime: '00:30',
      },
      {
        name: 'Company name',
        image: null,
        rating: 2,
        price: '500',
        remainingTime: '54:12',
      },
    ],
  },
  {
    id: '90380938',
    name: 'John Doe',
    responses: [],
    orderType: 'Furniture',
    remainingTime: '00:12',
    timeOut: true,
    orderDate: '28 AUG 2020',
  },
  {
    id: '90380938',
    name: 'John Doe',
    orderType: 'Furniture',
    remainingTime: '54:12',
    orderDate: '28 AUG 2020',
    responses: [
      {
        name: 'The Golden Transportation',
        image: Images.dummyImages.driverAvatar,
        rating: 3,
        price: '800',
        remainingTime: '54:12',
      },
      {
        name: 'Company name',
        image: Images.dummyImages.driverAvatar2,
        rating: 4,
        price: '800',
        remainingTime: '54:12',
      },
      {
        name: 'Company name',
        image: null,
        rating: 2,
        price: '800',
        remainingTime: '54:12',
      },
    ],
  },
];

export const customerOnGoingOrders = [
  {
    id: '90380938',
    title: 'Delivered',
    status: ONGOING_ORDER_STATUS.DELIVERED,
    orderType: 'Furniture',
    price: '500',
    orderDate: '28 AUG 2020',
    //  isSpecial: true,
    user: {
      name: 'The Golden Transportation',
      image: Images.dummyImages.driverAvatar2,
      rating: 4,
    },
  },
  {
    id: '90380938',
    title: 'Not started',
    status: ONGOING_ORDER_STATUS.NOT_STARTED,
    orderType: 'Furniture',
    price: '500',
    orderDate: '28 AUG 2020',
    user: {
      name: 'The Golden Transportation',
      image: Images.dummyImages.driverAvatar2,
      rating: 4,
    },
  },
  {
    id: '90380938',
    title: 'On the way',
    status: ONGOING_ORDER_STATUS.ON_THE_WAY,
    orderType: 'Furniture',
    price: '500',
    orderDate: '28 AUG 2020',
    user: {
      name: 'The Golden Transportation',
      image: Images.dummyImages.driverAvatar2,
      rating: 4,
    },
  },
  {
    id: '90380938',
    title: 'Picked up',
    status: ONGOING_ORDER_STATUS.PICKED_UP,
    orderType: 'Furniture',
    price: '500',
    orderDate: '28 AUG 2020',
    user: {
      name: 'The Golden Transportation',
      image: Images.dummyImages.driverAvatar2,
      rating: 4,
    },
  },
];

export const driverNewPendingOrders = [
  {
    id: '90380938',
    orderType: 'Furniture',
    remainingTime: '54:12',
    orderDate: '28 AUG 2020',
    isActive: true,
    isSpecial: true,
    user: {
      name: 'TAM special shipment',
      image: Images.dummyImages.tamAvatar,
      rating: 4,
    },
    title: 'New',
    status: ONGOING_ORDER_STATUS.DELIVERED,
  },
  {
    id: '90380938',
    orderType: 'Furniture',
    remainingTime: '54:12',
    orderDate: '28 AUG 2020',
    isActive: true,
    isSpecial: false,
    user: {
      name: 'Ahmad mahmoud',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
    title: 'New',
    status: ONGOING_ORDER_STATUS.DELIVERED,
  },
];

export const driverNewOrders = [
  {
    id: '90380938',
    orderType: 'Furniture',
    remainingTime: '54:12',
    orderDate: '28 AUG 2020',
    isActive: false,
    isSpecial: false,
    user: {
      name: 'Ahmad mahmoud',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
    title: 'Delivered',
    status: ONGOING_ORDER_STATUS.DELIVERED,
  },
  {
    id: '90380938',
    orderType: 'Furniture',
    remainingTime: '54:12',
    orderDate: '28 AUG 2020',
    isActive: true,
    isSpecial: false,
    user: {
      name: 'Ahmad mahmoud',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
    title: 'Delivered',
    status: ONGOING_ORDER_STATUS.DELIVERED,
  },
];
export const driverPendingOrders = [
  {
    id: '90380938',
    orderType: 'Furniture',
    remainingTime: '54:12',
    price: '500',
    isActive: false,
    isSpecial: false,
    user: {
      name: 'Ahmad mahmoud',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
    title: 'Delivered',
    status: ONGOING_ORDER_STATUS.DELIVERED,
  },
  {
    id: '90380938',
    orderType: 'Furniture',
    remainingTime: '54:12',
    price: '500',
    isActive: true,
    isSpecial: false,
    user: {
      name: 'Ahmad mahmoud',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
    title: 'Delivered',
    status: ONGOING_ORDER_STATUS.DELIVERED,
  },
];
export const driverAcceptedOrders = [
  {
    id: '90380938',
    orderType: 'Furniture',
    price: '500',
    orderDate: '28 AUG 2020',
    isActive: false,
    isSpecial: false,
    user: {
      name: 'Ahmad mahmoud',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
    title: 'Accepted',
    status: ONGOING_ORDER_STATUS.DELIVERED,
  },
  {
    id: '90380938',
    orderType: 'Furniture',
    price: '500',
    orderDate: '28 AUG 2020',

    isActive: true,
    isSpecial: false,
    user: {
      name: 'Ahmad mahmoud',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
    title: 'Accepted',
    status: ONGOING_ORDER_STATUS.DELIVERED,
  },
];
export const driverOnGoingOrders = [
  {
    id: '90380938',
    orderType: 'Furniture',
    remainingTime: '54:12',
    orderDate: '28 AUG 2020',
    price: '500',
    isActive: true,
    isSpecial: false,
    title: 'On the way',
    status: ONGOING_ORDER_STATUS.ON_THE_WAY,
    user: {
      name: 'Ahmad mahmoud',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
  },
  {
    id: '90380938',
    orderType: 'Furniture',
    remainingTime: '54:12',
    orderDate: '28 AUG 2020',
    price: '500',
    isActive: true,
    isSpecial: false,
    title: 'Picked Up',
    status: ONGOING_ORDER_STATUS.PICKED_UP,

    user: {
      name: 'Ahmad mahmoud',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
  },
  {
    id: '90380938',
    orderType: 'Furniture',
    remainingTime: '54:12',
    price: '500',
    isActive: true,
    isSpecial: false,
    title: 'Delivered',
    status: ONGOING_ORDER_STATUS.DELIVERED,

    user: {
      name: 'Ahmad mahmoud',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
  },
];

// export const FaqsData = [
//   {
//     title: 'Question 1',
//     content: 'Lorem ipsum dolor sit amet, consectetur  adipiscing elit',
//   },
//   {
//     title: 'Question 2',
//     content:
//       'Lorem ipsum dolor sit amet, consectetur  adipiscing elit, Lorem ipsum dolor sit amet, consectetur  adipiscing elit, Lorem ipsum dolor sit amet, consectetur  adipiscing elit',
//   },
//   {
//     title: 'Question 3',
//     content: 'Lorem ipsum dolor sit amet, consectetur  adipiscing elit',
//   },
// ];

export const FaqsData = [
  {
    faqs: [
      {
        id: 1,
        question: 'Question Test 1',
        answer: 'Answer Test 1',
      },
      {
        id: 2,
        question: 'Question Test 2',
        answer: 'Answer Test 2',
      },
    ],
    faq_category: 'Shipping',
  },
  {
    faqs: [
      {
        id: 3,
        question: 'Question Test 3',
        answer: 'Answer Test 3',
      },
      {
        id: 4,
        question: 'Question Test 4',
        answer: 'Answer Test 4',
      },
      {
        id: 5,
        question: 'Question Test 4',
        answer: 'Answer Test 4',
      },
    ],
    faq_category: 'Test',
  },
];

export const OpenedMessages = [
  {
    subject: 'Message subject',
    message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit..',
    date: '16 May 2020 ',
    isNew: true,
  },
  {
    subject: 'Message subject',
    message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit..',
    date: '16 May 2020 ',
    isNew: false,
  },
];

export const closedMessages = [
  {
    subject: 'Message subject',
    message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit..',
    date: '16 May 2020 ',
    isNew: false,
  },
  {
    subject: 'Message subject',
    message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit..',
    date: '16 May 2020 ',
    isNew: false,
  },
];

export const CategoryData = [
  {
    id: 1,
    image: Images.icons.furniture,
    title: 'Plants',
  },
  {
    id: 2,
    image: Images.icons.furniture,
    title: 'Food',
  },
  {
    id: 3,
    image: Images.icons.furniture,
    title: 'Furniture',
  },
  {
    id: 4,
    image: Images.icons.furniture,
    title: 'Water',
  },
  {
    id: 5,
    image: Images.icons.furniture,
    title: 'Gas',
  },
];

export const LanguageData = [
  {
    id: 'ar',
    title: 'Arabic',
  },
  {
    id: 'en',
    title: 'English',
  },
];

export const VehicleDriversListData = [
  {
    id: '1',
    licenseType: DRIVER_LICENSE_TYPE.ACTIVE,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '2',
    licenseType: DRIVER_LICENSE_TYPE.IN_ACTIVE,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '3',
    licenseType: DRIVER_LICENSE_TYPE.BLOCKED,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '4',
    licenseType: DRIVER_LICENSE_TYPE.ACTIVE,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '6',
    licenseType: DRIVER_LICENSE_TYPE.BLOCKED,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '5',
    licenseType: DRIVER_LICENSE_TYPE.IN_ACTIVE,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '9',
    licenseType: DRIVER_LICENSE_TYPE.BLOCKED,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '7',
    licenseType: DRIVER_LICENSE_TYPE.ACTIVE,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '8',
    licenseType: DRIVER_LICENSE_TYPE.IN_ACTIVE,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '10',
    licenseType: DRIVER_LICENSE_TYPE.ACTIVE,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '11',
    licenseType: DRIVER_LICENSE_TYPE.IN_ACTIVE,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '12',
    licenseType: DRIVER_LICENSE_TYPE.BLOCKED,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '13',
    licenseType: DRIVER_LICENSE_TYPE.ACTIVE,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '14',
    licenseType: DRIVER_LICENSE_TYPE.IN_ACTIVE,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
  {
    id: '15',
    licenseType: DRIVER_LICENSE_TYPE.BLOCKED,
    name: 'John Brady',
    dob: '25-05-1997',
    iss: '12.03.2012',
    gender: 'M',
    exp: '12.08.2020',
    class: 'B',
    date: '12 Jan 2022',
  },
];
export const DriverOrderHistory = [
  {
    id: '90380938',
    title: 'Delivered',
    orderType: 'Furniture',
    orderDate: '28 AUG 2020',
    price: '500',
    user: {
      name: 'Customer name',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
  },
  {
    id: '90380938',
    title: 'Canceled',
    orderType: 'Furniture',
    orderDate: '28 AUG 2020',
    price: '500',
    isSpecial: true,
    user: {
      name: 'TAM special shipment',
      image: Images.dummyImages.tamAvatar,
    },
  },
  {
    id: '90380938',
    title: 'No Delivered',
    orderType: 'Furniture',
    orderDate: '28 AUG 2020',
    price: '500',
    user: {
      name: 'Customer name',
      image: Images.dummyImages.customerAvatar,
      rating: 4,
    },
  },
];

export const CustomerOrderHistory = [
  {
    id: '90380938',
    title: 'Delivered',
    orderType: 'Furniture',
    orderDate: '28 AUG 2020',
    price: '500',
    user: {
      name: 'The Golden transportation',
      image: Images.dummyImages.driverAvatar,
      rating: 4,
    },
  },
  {
    id: '90380938',
    title: 'Canceled',
    orderType: 'Furniture',
    orderDate: '28 AUG 2020',
    price: '500',
    user: {
      name: 'Company name',
      image: Images.dummyImages.driverAvatar2,
      rating: 4,
    },
  },
  {
    id: '90380938',
    title: 'No Delivered',
    orderType: 'Furniture',
    orderDate: '28 AUG 2020',
    price: '500',
    user: {
      name: 'Customer name',
      image: Images.dummyImages.driverAvatar,
      rating: 4,
    },
  },
];

export const OrderDetail = {
  data: [
    {
      quantity: '2',
      photos: [Images.dummyImages.sofaDummy, Images.dummyImages.sofaDummy],
      weight: '12 pounds',
      dimension: '37.5 x 600 x 20',
    },
    {
      quantity: '5',
      photos: [
        Images.dummyImages.sofaDummy,
        Images.dummyImages.sofaDummy,
        Images.dummyImages.sofaDummy,
        Images.dummyImages.sofaDummy,
        Images.dummyImages.sofaDummy,
      ],
      weight: '',
      dimension: '',
    },
  ],
  from: 'Address: 1004, Fortune Executive towers, Jumeirah lakes tower, Dubai',
  to: 'Address: 1004, Fortune Executive towers,Jumeirah lakes tower, Dubai',
  pickupTime: '28 AUG - Morning ( 9:00 AM - 12:00 PM )',
  deliverTime: '28 AUG - Morning ( 9:00 AM - 12:00 PM )',
  extraServices: [
    {
      service: 'Service 1',
    },
    {
      service: 'Service 2',
    },
    {
      service: 'Service 3',
    },
    {
      service: 'Service 4',
    },
    {
      service: 'Service 5',
    },
    {
      service: 'Service 6',
    },
  ],
};

export const DummyImageViewData = [
  Images.dummyImages.whiteSofaDummy,
  Images.dummyImages.whiteSofaDummy2,
  Images.dummyImages.whiteSofaDummy3,
];

export const VehicleTypeData = [
  {
    title: 'Normal Vehicles',
  },
  {
    title: 'Normal Vehicles',
  },
  {
    title: 'Normal Vehicles',
  },
];

export const VehicleCategoryData = [
  {
    title: 'Pickup (Single Cab)',
  },
  {
    title: 'Pickup (Double Cab)',
  },
];

export const VehicleColorData = [
  {
    title: 'Blue',
  },
  {
    title: 'Red',
  },
  {
    title: 'Yellow',
  },
];

export const MyVehicleDummyData = {
  id: '36985741',
  date: '28 AUG 20 ',
  vehicleType: 'Normal Vehicles',
  vehicleCategory: 'Pickup (Single cab)',
  vehicleColor: 'Blue',
  insuranceOfVehicle: [
    Images.dummyImages.sofaDummy,
    Images.dummyImages.sofaDummy,
    Images.dummyImages.sofaDummy,
  ],
  driverName: 'John Doe',
  driverLicense: [
    Images.dummyImages.sofaDummy,
    Images.dummyImages.sofaDummy,
    Images.dummyImages.sofaDummy,
  ],
  itemInsurance: 'Yes',
};

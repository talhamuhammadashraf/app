/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

// import SplashScreen from 'react-native-splash-screen';
// import {Provider} from 'react-redux';
//import { SafeAreaProvider } from "react-native-safe-area-context";
import {View, SafeAreaView, StatusBar} from 'react-native';
import React from 'react';
import {Colors} from './theme';

// import {/* ConfigureApp,*/ DataHandler} from './utils/';
// import {MessageBar} from './components';
// import { SplashVideo } from "./common";
import AppNavigator from './navigator';
// import configureStore from './store';
import {AppStyles} from './theme';
// import { requestPermission, registerFCMListener } from "./utils/FirebaseUtils";

//ConfigureApp();

export default class App extends React.Component {
  constructor(props) {
    super(props);

    // configureStore((store) => {
    //   Geocoder.init(GOOGLE_MAP_KEY);
    //   DataHandler.setStore(store);

    //   // this.setState({isLoading: false, store});
    //   setTimeout(() => {
    //     this.setState({isLoading: false, store}, () => {
    //       SplashScreen.hide();
    //     });
    //   }, 2000);
    // });
  }

  state = {
    isLoading: true,
    store: null,
  };

  UNSAFE_componentWillMount() {
    //console.log('Hello splash');
    // SplashScreen.hide();
  }

  render() {
    // if (this.state.isLoading) {
    //   return null;
    // }
    // alert('fsdfs');
    return (
      <View style={AppStyles.flex}>
        {/* <Provider store={this.state.store}> */}
          <AppNavigator />
        {/* </Provider> */}
        {/* <MessageBar /> */}
        <StatusBar backgroundColor={Colors.transparent} translucent={true} />
      </View>
    );
  }
}

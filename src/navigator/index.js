import * as React from 'react';
import {SafeAreaView} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
// import {connect} from 'react-redux';
import {NavigationService, Util} from '../utils';
//import { Colors, Images, Fonts } from "../theme";

const Stack = createStackNavigator();

import {
  Login,
  Home,

} from '../containers';

/*function getNavbarOptions(
  title,
  bgColor = Colors.backgroundColor,
  isHeaderTransparent = false,
) {
  return {
    headerTitleStyle: {
      // textAlign: "center",
      // alignSelf: "center",
      // flex: 1,
    },
    // headerBackImage: () => (
    //   <BackButton imageStyle={{tintColor: Colors.black}} />
    // ),
    // headerStyle: {
    //   elevation: 0,
    //   shadowOffset: {height: 0},
    //   backgroundColor: bgColor,
    // },
    // headerLayoutPreset: "center",
    //  headerTitle: () => <HeaderTitle title={title} />,
    headerTransparent: isHeaderTransparent,
    // gestureEnabled: false,
  };
}*/

class App extends React.Component {
  authStacks() {
    return (
      <NavigationContainer
        ref={(navigatorRef) => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}>
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  homeStacks() {
    return (
      <NavigationContainer
        ref={(navigatorRef) => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen
            name="Home"
            component={Home}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  render() {
    return this.authStacks()
    // const {user} = this.props;
    // return !Util.isEmpty(user) && user.phone_verified
    //   ? this.homeStacks()
    //   : this.authStacks();
  }
}

// const mapStateToProps = (store) => ({
//   user: store.auth.data,
// });

// export default connect(mapStateToProps, null)(App);
export default App;

// // @flow
import {StyleSheet} from 'react-native';
import {Metrics, Fonts, Colors} from '../theme';

export default StyleSheet.create({
  flex: {
    flex: 1,
  },
  containerViewTitleStyle: {
    fontSize: Fonts.size.size_24,
    color: Colors.dusk,
    fontFamily: Fonts.type.regular,
    lineHeight: 29,
    marginBottom: Metrics.ratio(10),
  },
  // headerRightContainer: {
  //   paddingRight: Metrics.ratio(20),
  // },
  // headerTitleStyle: {
  //   fontSize: Fonts.size.size_18,
  //   color: Colors.black,
  // },
  // backButtonStyle: {
  //   paddingVertical: 10,
  //   paddingHorizontal: 20,
  // },
  // rightButtonStyle: {
  //   paddingVertical: 10,
  //   paddingHorizontal: 20,
  // },
  // bottomButton: {
  //   marginHorizontal: Metrics.ratio(20),
  //   marginBottom: Metrics.buttonBottomPadding,
  // },
  // //gender
  // collapsedGenderInputStyle: {
  //   marginBottom: 16,
  //   borderRadius: 10,
  // },
  // genderInputStyle: {
  //   marginBottom: 0,
  //   borderTopLeftRadius: 10,
  //   borderTopRightRadius: 10,
  //   borderBottomLeftRadius: 0,
  //   borderBottomRightRadius: 0,
  // },
  // // DOB
  // collapsedDOBInputStyle: {
  //   marginTop: 0,
  // },
  // dobInputStyle: {
  //   marginTop: 16,
  // },
});

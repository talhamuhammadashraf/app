/* eslint-disable no-console */
// @flow

import {create} from 'apisauce';
import {
  API_LOG,
  BASE_URL,
  API_TIMEOUT,
  ERROR_SOMETHING_WENT_WRONG,
  ERROR_NETWORK_NOT_AVAILABLE,
  ACCESS_TOKEN_EXPIRED,
  TOKEN_EXPIRE,
} from '../config/WebService';
import Util from './Util';
import DataHandler from './DataHandler';
import {strings} from '../utils/i18n';
import auth, {authSelectors} from '../ducks/auth';
import {REQUEST_METHOD} from '../config/Constants';
import NavigationService from '../utils/NavigationService';
import {successLogout} from '../ducks/auth/actions';

const api = create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'form-data',
  },
  timeout: API_TIMEOUT,
});

const postFileApi = create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'multipart/form-data',
  },
  timeout: API_TIMEOUT,
});

// defaultErrorShow = {showErrorMsg: 0, showSuccessMsg: 0};

class ApiSauce {
  getHeaders() {
    const store = DataHandler.getStore();
    const defaultHeader = {};

    if (store) {
      // const {user} = store.getState();
      const accessToken = authSelectors.getUserToken(store);

      // const userData = selectUserData(user);

      // get login access token if user authenticated
      if (!Util.isEmpty(accessToken)) {
        const header = {
          'X-Access-Token': accessToken,
          ...defaultHeader,
        };
        return header;
      }

      if (DataHandler.getTempUser()) {
        const header = {
          'X-Access-Token': DataHandler.getTempUser().accessToken,
        };
        return header;
      }
    }
    return defaultHeader;
  }

  getHeadersForFile() {
    const store = DataHandler.getStore();
    const defaultHeader = {
      'Content-Type': 'multipart/form-data',
    };

    if (store) {
      // const {user} = store.getState();
      const accessToken = authSelectors.getUserToken(store);
      // const userData = selectUserData(user);
      //console.log('accessToken 1', accessToken);
      // get login access token if user authenticated
      if (!Util.isEmpty(accessToken)) {
        //console.log('accessToken 2', accessToken);
        const header = {
          'X-Access-Token': accessToken,
          ...defaultHeader,
        };

        return header;
      }

      // get 'remember token' as temp token initial user
      // if (!Util.isEmpty(userData.rememberToken)) {
      //   const header = {
      //     'X-Access-Token': userData.rememberToken,
      //     ...defaultHeader,
      //   };

      //   return header;
      // }
    }
    return defaultHeader;
  }

  async callRequest(
    url,
    data,
    methodType,
    errorSuccessNotification = {},
    headers,
    onProgressUpdate = null,
  ) {
    // eslint-disable-next-line default-case
    switch (methodType) {
      case REQUEST_METHOD.POST:
        return this.post(url, data, errorSuccessNotification, onProgressUpdate);
      case REQUEST_METHOD.FILE:
        return this.postFile(url, data, errorSuccessNotification);
      case REQUEST_METHOD.GET:
        return this.get(url, data, errorSuccessNotification);
      case REQUEST_METHOD.DELETE:
        return this.delete(url, data, errorSuccessNotification);
      case REQUEST_METHOD.PUT:
        return this.put(url, data, errorSuccessNotification);
      case REQUEST_METHOD.PATCH:
        return this.patch(url, data, errorSuccessNotification);
    }
  }

  async post(
    url,
    data,
    errorSuccessNotification = {},
    onProgressUpdate,
    headers = this.getHeaders(),
    isFormUrlEncoded = false,
  ) {
    // const payload = this.getPayload(data);
    //const { showErrorMsg, showSuccessMsg, ...rest } = data;

    // const params = isFormUrlEncoded ? qs.stringify(data) : data;
    const timeout = data.timeout || API_TIMEOUT;

    // if (__DEV__ && API_LOG) {
    //   console.log('params ==>>', data);
    //   console.log('headers ==>>', headers);
    // }

    // const response = await api.post(url, data, {headers, timeout});
    const response = await api.post(url, data, {
      headers,
      timeout,
      onUploadProgress: (e) => {
        console.log(e);
        if (onProgressUpdate) {
          if (e.lengthComputable) {
            const progress = (e.loaded / e.total) * 100;
            console.log(
              'progress ==>>',
              progress,
              ' ==>> toFixed ==>> ',
              progress.toFixed(0),
            );
            onProgressUpdate(progress.toFixed(0));
          }
        }
      },
    });

    if (__DEV__ && API_LOG) {
      console.log('API -> url -> ', url);
      console.log('API -> Payload ->', data);
      console.log('API -> Headers -> ', headers);
      console.log('API -> Response -> ', JSON.stringify(response));
    }

    return this.handleResponse(response, data, errorSuccessNotification);
  }

  async get(
    url,
    data,
    errorSuccessNotification = {},
    headers = this.getHeaders(),
  ) {
    // let payload = this.getPayload(data);
    const response = await api.get(url, data, {headers});

    if (__DEV__ && API_LOG) {
      console.log('API -> url -> ', url);
      console.log('API -> Payload ->', data);
      console.log('API -> Headers -> ', headers);
      console.log('API -> Response -> ', JSON.stringify(response));
    }

    return this.handleResponse(response, data, errorSuccessNotification);
  }

  async delete(
    url,
    data,
    errorSuccessNotification = {},
    headers = this.getHeaders(),
  ) {
    // const payload = this.getPayload(data);
    //const { showErrorMsg, showSuccessMsg, ...rest } = data;
    const response = await api.delete(url, data, {headers});

    if (__DEV__ && API_LOG) {
      console.log('API -> url -> ', url);
      console.log('API -> Payload ->', data);
      console.log('API -> Headers -> ', headers);
      console.log('API -> Response -> ', JSON.stringify(response));
    }

    return this.handleResponse(response, data, errorSuccessNotification);
  }

  async put(
    url,
    data,
    errorSuccessNotification = {},
    headers = this.getHeaders(),
  ) {
    // const payload = this.getPayload(data);
    //const { showErrorMsg, showSuccessMsg, ...rest } = data;
    const response = await api.put(url, data, {headers});

    if (__DEV__ && API_LOG) {
      console.log('API -> url -> ', url);
      console.log('API -> Payload ->', data);
      console.log('API -> Headers -> ', headers);
      console.log('API -> Response -> ', JSON.stringify(response));
    }

    return this.handleResponse(response, data, errorSuccessNotification);
  }

  async patch(
    url,
    data,
    errorSuccessNotification = {},
    headers = this.getHeaders(),
  ) {
    // const payload = this.getPayload(data);
    //const { showErrorMsg, showSuccessMsg, ...rest } = data;
    const response = await api.patch(url, data, {headers});

    if (__DEV__ && API_LOG) {
      console.log('API -> url -> ', url);
      console.log('API -> Payload ->', data);
      console.log('API -> Headers -> ', headers);
      console.log('API -> Response -> ', JSON.stringify(response));
    }

    return this.handleResponse(response, data, errorSuccessNotification);
  }

  // for uploading images
  async postFile(
    url,
    data,
    errorSuccessNotification = {},
    headers = this.getHeadersForFile(),
  ) {
    const response = await postFileApi.post(url, data, {headers});

    if (__DEV__ && API_LOG) {
      console.log('API -> url -> ', url);
      console.log('API -> Payload ->', data);
      console.log('API -> Headers -> ', headers);
      console.log('API -> Response -> ', JSON.stringify(response));
    }

    return this.handleResponse(response, data, errorSuccessNotification);

    // if (__DEV__ && API_LOG) {
    //   console.log('API -> url -> ', url);
    //   console.log('API -> Payload ->', data);
    //   console.log('API -> Headers -> ', headers);
    //   console.log('API -> Response -> ', JSON.stringify(response));
    // }
    // return new Promise((resolve, reject) => {
    //   if (response.ok && response.data && !response.data.error) {
    //     resolve(response.data);
    //   } else {
    //     if (response.status === 500) {
    //       reject(ERROR_SOMETHING_WENT_WRONG);
    //     }
    //     reject(response.data || ERROR_SOMETHING_WENT_WRONG);
    //   }
    // });
  }

  showServerError(message, errorSuccessNotification) {
    if (
      !Util.isEmpty(errorSuccessNotification) &&
      // !Util.isEmpty(errorSuccessNotification.showErrorMsg) &&
      errorSuccessNotification.showErrorMsg === 1 &&
      !Util.isEmpty(message)
    ) {
      Util.showMessage(message);
    }
  }

  showServerSuccessMsg(response, errorSuccessNotification) {
    if (
      errorSuccessNotification &&
      errorSuccessNotification.showSuccessMsg &&
      errorSuccessNotification.showSuccessMsg == 1 &&
      !Util.isEmpty(response.data.message)
    ) {
      Util.showMessage(response.data.message, 'success');
    }
  }

  handleResponse(response, payload, errorSuccessNotification) {
    return new Promise((resolve, reject) => {
      if (response.ok && response.data && response.status === 200) {
        // show success msg if required
        this.showServerSuccessMsg(response, errorSuccessNotification);
        resolve(response.data);
      } else {
        let message = '';
        let responseObject;
        let token_expired = false;
        if (response.status === 403) {
          responseObject = ACCESS_TOKEN_EXPIRED;
          message = strings('api_error_messages.token_expired');
          token_expired = true;
        } else if (response.data && typeof response.data.message === 'object') {
          responseObject = response.data;
          message = response.data.message.message;
        } else if (response.data && response.data.message) {
          responseObject = response.data;
          message = response.data.message;
        } else if (typeof response.data === 'string') {
          responseObject = response.data;
          message = response.data;
        } else if (response.problem === 'CLIENT_ERROR') {
          responseObject = response.data;
          message = strings('api_error_messages.something_went_wrong');
        }
        // handle 500 error
        else if (response.status === 500) {
          responseObject = ERROR_SOMETHING_WENT_WRONG;
          message = strings('api_error_messages.something_went_wrong');
        }
        // handle network error
        else if (response.problem === 'NETWORK_ERROR') {
          responseObject = ERROR_NETWORK_NOT_AVAILABLE;
          message = strings('api_error_messages.network_not_available');
        } else {
          responseObject = ERROR_SOMETHING_WENT_WRONG;
          message = strings('api_error_messages.something_went_wrong');
        }

        // Util.showMessage("Test ...")

        setTimeout(() => {
          this.showServerError(message, errorSuccessNotification);
        }, 300);

        console.log('token_expired ==>>', token_expired);
        console.log('message ==>>', message);
        console.log('errorSuccessNotification ==>>', errorSuccessNotification);

        reject(responseObject);
        if (token_expired) {
          DataHandler.getStore().dispatch(successLogout());
        }
      }
    });
  }
}

export default new ApiSauce();

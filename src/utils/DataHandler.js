let store;
let usertype;
let isLogin = false;
let tempUser;

export default {
  setStore(value) {
    store = value;
  },

  getStore() {
    return store;
  },

  setUserLogin(is_login) {
    isLogin = is_login;
  },

  isUserLogin() {
    return isLogin;
  },

  setUserType(user) {
    usertype = user;
  },

  userType() {
    return usertype;
  },

  setTempUser(user) {
    tempUser = user;
  },

  getTempUser() {
    return tempUser;
  },
};

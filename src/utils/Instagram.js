const INSTAGRAM_CLIENT_ID = '265889331502868';
const INSTAGRAM_SCOPES = ['user_profile'];
const redirectUrl = 'https://www.cubix.co/';
const INSTAGRAM_CLIENT_SECRET_ID = '03d417372598b092446695127ce7bfba';
const GRANT_TYPE = 'authorization_code';

class Instagram {
  GetClientID() {
    return INSTAGRAM_CLIENT_ID;
  }

  GetClientScope() {
    return INSTAGRAM_SCOPES;
  }

  GetRedirectURL() {
    return redirectUrl;
  }

  GetAccessToken(accessCode, callBack) {
    fetch(`https://api.instagram.com/oauth/access_token`, {
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      method: 'POST',
      body: `app_id=${INSTAGRAM_CLIENT_ID}&app_secret=${INSTAGRAM_CLIENT_SECRET_ID}&grant_type=${GRANT_TYPE}&redirect_uri=${redirectUrl}&code=${accessCode}`,
    })
      .then((response) => response.json())
      .then((responseJson) => {
        callBack(responseJson, null);
      })
      .catch((error) => {
        callBack(null, error);
      });
  }

  GetUserDetail(accessTokenJSON, callback) {
    fetch(
      `https://graph.instagram.com/me?fields=id,username&access_token=${accessTokenJSON.access_token}`,
    )
      .then((response) => response.json())
      .then((responseJson) => {
        callback(responseJson, null);
      })
      .catch((error) => {
        callback(null, error);
      });
  }

  logout(callBack) {
    fetch(`http://instagram.com/accounts/logout/`)
      .then((response) => response.text())
      .then((responseText) => {
        callBack();
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

export default new Instagram();

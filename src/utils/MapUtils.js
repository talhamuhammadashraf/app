import Geolocation from '@react-native-community/geolocation';
import {Alert, PermissionsAndroid, Platform, Linking} from 'react-native';
import Geocoder from 'react-native-geocoding';

async function hasLocationPermissions() {
  if (
    Platform.OS === 'ios' ||
    (Platform.OS === 'android' && Platform.Version < 23)
  ) {
    return true;
  }

  const hasPermission = await PermissionsAndroid.check(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );

  if (hasPermission) {
    return true;
  }

  const status = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  );

  if (status === PermissionsAndroid.RESULTS.GRANTED) {
    return true;
  }

  // status is PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN is handled on error response thats why allow
  if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
    return true;
  }

  // else status is PermissionsAndroid.RESULTS.DENIED then return false
  return false;
}

function openSettingModal() {
  Alert.alert(
    'Permission required',
    'Need permissions to access location',
    [
      {
        text: 'Cancel',
        style: 'cancel',
      },
      {
        text: 'Open Settings',
        onPress: () => Linking.openSettings(),
      },
    ],
    {cancelable: false},
  );
}

export async function getLocation() {
  const hasLocationPermission = await hasLocationPermissions();

  if (!hasLocationPermission) {
    openSettingModal();
  }
  // alert(JSON.stringify(hasLocationPermission));

  return new Promise((resolve, reject) => {
    Geolocation.getCurrentPosition(
      (position) => {
        resolve(position.coords);
      },
      (error) => {
        if (error.code === 1) {
          openSettingModal();
        } else {
          reject(error.message);
        }
      },
      {
        enableHighAccuracy: false,
        timeout: 30000,
        maximumAge: 10000,
      },
    );
  });
}

export function getUpdatedRegion(region, setLocation) {
  Geocoder.from(region.latitude, region.longitude)
    .then((json) => {
      // var addressComponent = json.results[0].address_components[0];
      // console.log(json);
      const latitude = json.results[0].geometry.location.lat;
      const longitude = json.results[0].geometry.location.lng;
      const street = json.results[0].formatted_address;
      setLocation(latitude, longitude, street);
    })
    .catch((error) => console.warn(error));
}

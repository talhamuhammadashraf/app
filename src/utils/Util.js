// import {Platform} from 'react-native';
// import _ from 'lodash';
// import moment from 'moment';
// import {MessageBarManager} from 'react-native-message-bar';

// import {DATE_FORMAT, CONTACT_US_DATE_FORMAT} from '../config/Constants';

// function isPlatformAndroid() {
//   return Platform.OS === 'android';
// }

// function isPlatformIOS() {
//   return Platform.OS === 'ios';
// }

// function getPlatform() {
//   return Platform.OS;
// }

// function isEmpty(data) {
//   return _.isEmpty(data);
// }

// function formatDate(dateString, currentDateFormat, FormattedDateFormat) {
//   return moment(dateString, currentDateFormat).format(FormattedDateFormat);
// }

// function getCurrentDate(format = DATE_FORMAT) {
//   return moment().format(format);
// }

// function getDate(date, format = CONTACT_US_DATE_FORMAT) {
//   return moment(date).format(format);
// }

// function showMessage(message, alertType = 'error', duration: 3000) {
//   MessageBarManager.showAlert({
//     message,
//     alertType,
//     duration,
//   });
// }

// function getTypeAuth(identifier) {
//   return `USER_${identifier}`;
// }

// export default {
//   showMessage,
//   isPlatformAndroid,
//   isPlatformIOS,
//   getPlatform,
//   isEmpty,
//   formatDate,
//   getCurrentDate,
//   getTypeAuth,
//   getDate,
// };
